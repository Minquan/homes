#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "homes.h"

// TODO: Write a function called readhomes that takes the filename
// of the homes file and returns an array of pointers to home structs
// terminated with a NULL
home **readHomes(char *filename)
{
    int size = 0;
    int zip;
    char addr[40];
    int price;
    int area;
    int count = 0;
    
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        printf("Can't open %s for reading.\n", filename);
        exit(1);
    }
    while(fscanf(f, "%i,%[^,],%i,%i\n", &zip, addr, &price, &area) != EOF)
    {
        size++;
    }
    fclose(f);
    
    
    f = fopen(filename, "r");
    if(!f)
    {
        printf("Can't open %s for reading.\n", filename);
        exit(1);
    }
    
    home **homes = (home **)malloc((size+1) * sizeof(home *));
    while(fscanf(f, "%i,%[^,],%i,%i\n", &zip, addr, &price, &area) != EOF)
    {   
        home *p = (home*)malloc(sizeof(home));
        p->zip = zip;
        p->addr = malloc(40);
        strcpy(p->addr, addr);
        p->price = price;
        p->area = area;
        
        homes[count] = p;
        count++;
    }
    
    fclose(f);
    
    homes[size] = NULL;
    
    return homes;
}

int getSize(home **homes)
{
    int i = 0;
    while (homes[i]!=NULL)
    {
        i++;
    }
    return i;
}

int getSameZip(home **homes, int zip)
{
    int i = 0;
    int count = 0;
    while(homes[i]!=NULL)
    {
        if(zip == homes[i]->zip)
        {
            count++;
        }
        i++;
    }
    
    return count;
}

home **getPriceRange(int p, home **homes)
{
    int i = 0;
    int k = 0;
    int size = 0;
    while(homes[i]!=NULL)
    {
        if((homes[i]->price) <= (p+10000) && (homes[i]->price) >= (p-10000))
        {
            size++;
        }
        i++;
    }
    if(size==0)return NULL;
    
    home **h = (home**)malloc((size+1)*sizeof(home*));
    i = 0;
    while(homes[i]!=NULL)
    {
        if((homes[i]->price) <= (p+10000) && (homes[i]->price) >= (p-10000))
        {
            h[k] = homes[i];
            k++;
        }
        i++;
    }
    h[size] = NULL;
    
    return h;
}

int main(int argc, char *argv[])
{
    // TODO: Use readhomes to get the array of structs
    home **homes = readHomes(argv[1]);

    // At this point, you should have the data structure built
    // TODO: How many homes are there? Write a function, call it,
    // and print the answer.
    int size = getSize(homes);
    printf("File has %d homes.\n",size);
    
    
    // TODO: Prompt the user to type in a zip code.
    int zip;
    printf("Enter a zip code: \n");
    scanf("%d",&zip);

    
    // At this point, the user's zip code should be in a variable
    // TODO: How many homes are in that zip code? Write a function,
    // call it, and print the answer.
    int count = getSameZip(homes,zip);
    printf("%d homes has this zip code.\n", count);
    

    // TODO: Prompt the user to type in a price.
    int price;
    printf("Enter a price: \n");
    scanf("%d",&price);
    

    // At this point, the user's price should be in a variable
    // TODO: Write a function to print the addresses and prices of 
    // homes that are within $10000 of that price.
    home **hPrice = getPriceRange(price, homes);
    int j = 0;
    if(hPrice!=NULL)
    {
        while(hPrice[j] != NULL)
        {
            printf("%s, %i\n", hPrice[j]->addr, hPrice[j]->price);
            j++;
        }
    }
    else
    {
        printf("Found none.\n");
    }
    
}
